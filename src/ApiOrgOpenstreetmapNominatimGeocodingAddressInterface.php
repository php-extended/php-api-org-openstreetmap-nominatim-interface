<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm;

use Stringable;

/**
 * ApiOrgOpenstreetmapNominatimGeocodingAddressInterface interface file.
 * 
 * This represents a geocoded address.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiOrgOpenstreetmapNominatimGeocodingAddressInterface extends Stringable
{
	
	/**
	 * Gets the amenity.
	 * 
	 * @return ?string
	 */
	public function getAmenity() : ?string;
	
	/**
	 * Gets the borough (arrondissement).
	 * 
	 * @return ?string
	 */
	public function getBorough() : ?string;
	
	/**
	 * Gets the number of the area in the street.
	 * 
	 * @return ?string
	 */
	public function getHouseNumber() : ?string;
	
	/**
	 * Gets the road.
	 * 
	 * @return ?string
	 */
	public function getRoad() : ?string;
	
	/**
	 * Gets the quarter.
	 * 
	 * @return ?string
	 */
	public function getQuarter() : ?string;
	
	/**
	 * Gets the neighbourhood name.
	 * 
	 * @return ?string
	 */
	public function getNeighbourhood() : ?string;
	
	/**
	 * Gets the district.
	 * 
	 * @return ?string
	 */
	public function getDistrict() : ?string;
	
	/**
	 * Gets the suburban area name.
	 * 
	 * @return ?string
	 */
	public function getSuburb() : ?string;
	
	/**
	 * Gets the county.
	 * 
	 * @return ?string
	 */
	public function getCounty() : ?string;
	
	/**
	 * Gets the city name.
	 * 
	 * @return ?string
	 */
	public function getCity() : ?string;
	
	/**
	 * Gets the city block name.
	 * 
	 * @return ?string
	 */
	public function getCityBlock() : ?string;
	
	/**
	 * Gets the state name.
	 * 
	 * @return ?string
	 */
	public function getState() : ?string;
	
	/**
	 * Gets the postal code.
	 * 
	 * @return ?string
	 */
	public function getPostCode() : ?string;
	
	/**
	 * Gets the country name.
	 * 
	 * @return ?string
	 */
	public function getCountry() : ?string;
	
	/**
	 * Gets the country code.
	 * 
	 * @return ?string
	 */
	public function getCountryCode() : ?string;
	
	/**
	 * Gets the ISO 3166-2 lvl4 value.
	 * 
	 * @return ?string
	 */
	public function getIso31662lvl4() : ?string;
	
}
