<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm;

use PhpExtended\GeoJson\GeoJsonBoundingBoxInterface;
use PhpExtended\GeoJson\GeoJsonGeometryInterface;
use Stringable;

/**
 * ApiOrgOpenstreetmapNominatimGeocodingResultInterface interface file.
 * 
 * This represents an object that can be obtained from the geocoding api of
 * openstreetmap nominatim.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiOrgOpenstreetmapNominatimGeocodingResultInterface extends Stringable
{
	
	/**
	 * Gets the full address of this object.
	 * 
	 * @return ?ApiOrgOpenstreetmapNominatimGeocodingAddressInterface
	 */
	public function getAddress() : ?ApiOrgOpenstreetmapNominatimGeocodingAddressInterface;
	
	/**
	 * Gets the bounding box that encloses the shape of this object.
	 * 
	 * @return ?GeoJsonBoundingBoxInterface
	 */
	public function getBoundingBox() : ?GeoJsonBoundingBoxInterface;
	
	/**
	 * Gets the class of the object.
	 * 
	 * @return string
	 */
	public function getClass() : string;
	
	/**
	 * Gets the full display name of the location of the object.
	 * 
	 * @return string
	 */
	public function getDisplayName() : string;
	
	/**
	 * Gets the importance of the object.
	 * 
	 * @return string
	 */
	public function getImportance() : string;
	
	/**
	 * Gets the latitude of the object.
	 * 
	 * @return string
	 */
	public function getLatitude() : string;
	
	/**
	 * Gets the licence text for using this api.
	 * 
	 * @return string
	 */
	public function getLicence() : string;
	
	/**
	 * Gets the longitude of the object.
	 * 
	 * @return string
	 */
	public function getLongitude() : string;
	
	/**
	 * Gets the id of the nearest osm object.
	 * 
	 * @return string
	 */
	public function getOsmId() : string;
	
	/**
	 * Gets the type of the nearest osm object.
	 * 
	 * @return string
	 */
	public function getOsmType() : string;
	
	/**
	 * Gets the id of the place this object is on.
	 * 
	 * @return string
	 */
	public function getPlaceId() : string;
	
	/**
	 * Gets the id of the place this object is ranked to.
	 * 
	 * @return string
	 */
	public function getPlaceRank() : string;
	
	/**
	 * Gets the svg string that shapes this object.
	 * 
	 * @return string
	 */
	public function getSvg() : string;
	
	/**
	 * Gets the category of the object.
	 * 
	 * @return string
	 */
	public function getCategory() : string;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return string
	 */
	public function getType() : string;
	
	/**
	 * Gets the type of the address.
	 * 
	 * @return ?string
	 */
	public function getAddresstype() : ?string;
	
	/**
	 * Gets the icon for this place.
	 * 
	 * @return string
	 */
	public function getIcon() : string;
	
	/**
	 * Gets the geojson polygon, in case asked for.
	 * 
	 * @return ?GeoJsonGeometryInterface
	 */
	public function getGeojson() : ?GeoJsonGeometryInterface;
	
	/**
	 * Gets the geokml polygon, in case asked for.
	 * 
	 * @return ?string
	 */
	public function getGeokml() : ?string;
	
	/**
	 * Gets the geotext polygon, in case asked for.
	 * 
	 * @return ?string
	 */
	public function getGeotext() : ?string;
	
	/**
	 * Gets the name of the locality.
	 * 
	 * @return ?string
	 */
	public function getName() : ?string;
	
	/**
	 * Gets the name details, if requested.
	 * 
	 * @return ?ApiOrgOpenstreetmapNominatimNameDetailsInterface
	 */
	public function getNameDetails() : ?ApiOrgOpenstreetmapNominatimNameDetailsInterface;
	
}
