<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm;

use PhpExtended\Email\EmailAddressInterface;
use PhpExtended\HttpClient\AcceptLanguageChainInterface;
use Stringable;

/**
 * ApiOrgOpenstreetmapNominatimReverseRequestInterface interface file.
 * 
 * This represents a query to geocode an address to coordinates.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiOrgOpenstreetmapNominatimReverseRequestInterface extends Stringable
{
	
	/**
	 * Gets preferred language order for showing search results, overrides the
	 * value specified in the "Accept-Language" HTTP header. Either uses
	 * standard rfc2616 accept-language string or a simple comma separated list
	 * of language codes.
	 * 
	 * @return ?AcceptLanguageChainInterface
	 */
	public function getAcceptLanguage() : ?AcceptLanguageChainInterface;
	
	/**
	 * Gets a specific osm node / way / relation to return an address for.
	 * Please use this in preference to lat/lon where possible.
	 * 
	 * @return ?string
	 */
	public function getOsmType() : ?string;
	
	/**
	 * Gets the id of the osm object.
	 * 
	 * @return ?string
	 */
	public function getOsmId() : ?string;
	
	/**
	 * Gets the location to generate an address for.
	 * 
	 * @return ?float
	 */
	public function getLatitude() : ?float;
	
	/**
	 * Gets the location to generate an address for.
	 * 
	 * @return ?float
	 */
	public function getLongitude() : ?float;
	
	/**
	 * Gets level of detail required where 0 is country and 18 is
	 * house/building.
	 * 
	 * @return ?int
	 */
	public function getZoom() : ?int;
	
	/**
	 * Gets whether to include a breakdown of the address into elements.
	 * 
	 * @return ?bool
	 */
	public function hasAddressDetails() : ?bool;
	
	/**
	 * Gets if you are making large numbers of request please include a valid
	 * email address or alternatively include your email address as part of the
	 * User-Agent string. This information will be kept confidential and only
	 * used to contact you in the event of a problem, see Usage Policy for more
	 * details.
	 * 
	 * @return ?EmailAddressInterface
	 */
	public function getEmail() : ?EmailAddressInterface;
	
	/**
	 * Gets whether to output geometry of results in geojson format.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonGeojson() : ?bool;
	
	/**
	 * Gets whether to output geometry of results in kml format.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonKml() : ?bool;
	
	/**
	 * Gets whether to output geometry of results in svg format.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonSvg() : ?bool;
	
	/**
	 * Gets whether to output geometry of results as a WKT.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonText() : ?bool;
	
	/**
	 * Gets whether to include additional information in the result if
	 * available, e.g. wikipedia link, opening hours.
	 * 
	 * @return ?bool
	 */
	public function hasExtraTags() : ?bool;
	
	/**
	 * Gets include a list of alternative names in the results. These may
	 * include language variants, references, operator and brand.
	 * 
	 * @return ?bool
	 */
	public function hasNameDetails() : ?bool;
	
}
