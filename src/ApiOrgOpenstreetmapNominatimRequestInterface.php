<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm;

use PhpExtended\Email\EmailAddressInterface;
use PhpExtended\HttpClient\AcceptLanguageChainInterface;
use Stringable;

/**
 * ApiOrgOpenstreetmapNominatimRequestInterface interface file.
 * 
 * This represents a query to geocode an address to coordinates.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiOrgOpenstreetmapNominatimRequestInterface extends Stringable
{
	
	/**
	 * Gets preferred language order for showing search results, overrides the
	 * value specified in the "Accept-Language" HTTP header. Either uses
	 * standard rfc2616 accept-language string or a simple comma separated list
	 * of language codes.
	 * 
	 * @return ?AcceptLanguageChainInterface
	 */
	public function getAcceptLanguage() : ?AcceptLanguageChainInterface;
	
	/**
	 * Gets query string to search for. If this is set, the values for all the
	 * attributes {street, postal_code, city, county, state, country} are
	 * erased.
	 * 
	 * @return ?string
	 */
	public function getQuery() : ?string;
	
	/**
	 * Gets street number and street name.
	 * 
	 * @return ?string
	 */
	public function getStreet() : ?string;
	
	/**
	 * Gets the postal code of the city.
	 * 
	 * @return ?string
	 */
	public function getPostalCode() : ?string;
	
	/**
	 * Gets the name of the city.
	 * 
	 * @return ?string
	 */
	public function getCity() : ?string;
	
	/**
	 * Gets the county of the query.
	 * 
	 * @return ?string
	 */
	public function getCounty() : ?string;
	
	/**
	 * Gets the state of the query.
	 * 
	 * @return ?string
	 */
	public function getState() : ?string;
	
	/**
	 * Gets the country of the query.
	 * 
	 * @return ?string
	 */
	public function getCountry() : ?string;
	
	/**
	 * Gets limit search results to a specific country (or a list of
	 * countries). <countrycode> should be the ISO 3166-1alpha2 code, e.g. gb
	 * for the United Kingdom, de for Germany, etc.
	 * 
	 * @return array<int, string>
	 */
	public function getCountryCodes() : array;
	
	/**
	 * Gets the preferred area to find search results. Any two corner points of
	 * the box are accepted in any order as long as they span a real box.
	 * 
	 * @return array<int, int>
	 */
	public function getViewbox() : array;
	
	/**
	 * Gets restrict the results to only items contained with the viewbox (see
	 * above).
	 * Restricting the results to the bounding box also enables searching by
	 * amenity only. For example a search query of just "[pub]" would normally
	 * be rejected but with bounded=1 will result in a list of items matching
	 * within the bounding box.
	 * 
	 * @return ?bool
	 */
	public function hasBounded() : ?bool;
	
	/**
	 * Gets whether to include a breakdown of the address into elements.
	 * 
	 * @return ?bool
	 */
	public function hasAddressDetails() : ?bool;
	
	/**
	 * Gets if you are making large numbers of request please include a valid
	 * email address or alternatively include your email address as part of the
	 * User-Agent string. This information will be kept confidential and only
	 * used to contact you in the event of a problem, see Usage Policy for more
	 * details.
	 * 
	 * @return ?EmailAddressInterface
	 */
	public function getEmail() : ?EmailAddressInterface;
	
	/**
	 * Gets if you do not want certain openstreetmap objects to appear in the
	 * search result, give a comma separated list of the place_id's you want to
	 * skip.
	 * This can be used to broaden search results. For example, if a previous
	 * query only returned a few results, then including those here would cause
	 * the search to return other, less accurate, matches (if possible).
	 * 
	 * @return array<int, int>
	 */
	public function getExcludePlaceIds() : array;
	
	/**
	 * Gets limit the number of returned results. Default is 10.
	 * 
	 * @return ?int
	 */
	public function getLimit() : ?int;
	
	/**
	 * Gets sometimes you have several objects in OSM identifying the same
	 * place or object in reality. The simplest case is a street being split in
	 * many different OSM ways due to different characteristics. Nominatim will
	 * attempt to detect such duplicates and only return one match; this is
	 * controlled by the dedupe parameter which defaults to 1. Since the limit
	 * is, for reasons of efficiency, enforced before and not after
	 * de-duplicating, it is possible that de-duplicating leaves you with less
	 * results than requested.
	 * 
	 * @return ?bool
	 */
	public function hasDedupe() : ?bool;
	
	/**
	 * Gets output assorted developer debug information. Data on internals of
	 * nominatim "Search Loop" logic, and SQL queries. The output is (rough)
	 * HTML format. This overrides the specified machine readable format.
	 * 
	 * @return ?bool
	 */
	public function hasDebug() : ?bool;
	
	/**
	 * Gets whether to output geometry of results in geojson format.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonGeojson() : ?bool;
	
	/**
	 * Gets whether to output geometry of results in kml format.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonKml() : ?bool;
	
	/**
	 * Gets whether to output geometry of results in svg format.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonSvg() : ?bool;
	
	/**
	 * Gets whether to output geometry of results as a WKT.
	 * 
	 * @return ?bool
	 */
	public function hasPolygonText() : ?bool;
	
	/**
	 * Gets whether to include additional information in the result if
	 * available, e.g. wikipedia link, opening hours.
	 * 
	 * @return ?bool
	 */
	public function hasExtraTags() : ?bool;
	
	/**
	 * Gets include a list of alternative names in the results. These may
	 * include language variants, references, operator and brand.
	 * 
	 * @return ?bool
	 */
	public function hasNameDetails() : ?bool;
	
}
