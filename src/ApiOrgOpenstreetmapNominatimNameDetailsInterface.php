<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm;

use Stringable;

/**
 * ApiOrgOpenstreetmapNominatimNameDetailsInterface interface file.
 * 
 * This represents the name and all its variations across languages.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiOrgOpenstreetmapNominatimNameDetailsInterface extends Stringable
{
	
	/**
	 * Gets the canonical name.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the names in other languages.
	 * 
	 * @return array<string, string>
	 */
	public function getNames() : array;
	
}
