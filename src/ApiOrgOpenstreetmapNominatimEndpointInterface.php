<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-org-openstreetmap-nominatim-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Osm;

use Iterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use Stringable;

/**
 * ApiOrgOpenstreetmapNominatimEndpointInterface class file.
 *
 * This implements is to get informations from the nominatim api.
 *
 * @author Anastaszor
 * @see https://wiki.Osm.org/wiki/Nominatim
 */
interface ApiOrgOpenstreetmapNominatimEndpointInterface extends Stringable
{
	
	public const OSM_TYPE_NODE = 'N';
	public const OSM_TYPE_WAY = 'Y';
	public const OSM_TYPE_RELATION = 'R';
	
	/**
	 * Gets the results of a geocoding request.
	 *
	 * @param ApiOrgOpenstreetmapNominatimRequestInterface $request
	 * @return Iterator<ApiOrgOpenstreetmapNominatimGeocodingResultInterface>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function searchGeocode(ApiOrgOpenstreetmapNominatimRequestInterface $request) : Iterator;
	
	/**
	 * Gets the result of a reverse geocoding request.
	 * 
	 * @param ApiOrgOpenstreetmapNominatimReverseRequestInterface $request
	 * @return ApiOrgOpenstreetmapNominatimGeocodingResultInterface
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function searchReverseGeocode(ApiOrgOpenstreetmapNominatimReverseRequestInterface $request) : ApiOrgOpenstreetmapNominatimGeocodingResultInterface;
	
}
