# php-extended/php-api-org-openstreetmap-nominatim-interface

A library that gets data from nominatim.openstreetmap.org website's api.

![coverage](https://gitlab.com/php-extended/php-api-org-openstreetmap-nominatim-interface/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-api-org-openstreetmap-nominatim-interface ^8`


## Basic Usage

This library is an interface-only library.

For a concrete implementation, see [`php-extended/php-api-org-openstreetmap-nominatim-object`](https://gitlab.com/php-extended/php-api-org-openstreetmap-nominatim-object).


## License

MIT (See [license file](LICENSE)).
